<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\DashboardNewsController;
use App\Http\Controllers\AdminKategoryController;

use App\Models\User;
use App\Models\Kategory;
use App\Models\News;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
     "title" => "Home"
    ]);
 });

 Route::get('/news', [NewsController::class, 'index']);
 // halaman siggle post
 Route::get('news/{new:slug}', [NewsController::class,'show']);

 Route::get('/kategories', function(){
    return view('kategories',[
        'title' => 'Post Categories',
        'kategories' => Kategory::all(),
    ]);
});


 Route::get('/login', [LoginController::class, 'login'])->name('login')->middleware('guest');
 Route::post('/login', [LoginController::class, 'authenticate']);
 Route::post('/logout', [LoginController::class, 'logout']);

 Route::get('/regis', [RegisController::class, 'index'])->middleware('guest');
Route::post('/regis', [RegisController::class, 'store']);


Route::get('/dashboard', function(){
    return view('dashboard.index');
})->middleware('auth');


Route::get('/dashboard/news/cekSlug',[DashboardNewsController::class,'cekSlug'])->middleware('auth');

Route::resource('/dashboard/news', DashboardNewsController::class)->middleware('auth');

Route::resource('/dashboard/kategories', AdminKategoryController::class)->except('show')->middleware('admin');


