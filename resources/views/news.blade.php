@extends('halaman.main')

@section('container')
<h1 class="mb-3 text-center">{{ $title }}</h1>
<div class="row justify-content-center mb-3">
    <div class="col-md-6">
        <form action="/news">
    @if (request('kategory'))
    <input type="hidden" name="kategory" value="{{ request('kategory') }}">
    @endif
    @if (request('author'))
    <input type="hidden" name="author" value="{{ request('author') }}">
    @endif
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Search..." name="search" value="{{ request('search') }}">
                <button class="btn btn-danger" type="sumbit">Search</button>
              </div>
        </form>
    </div>
</div>



@if ($news->count())
<div class="card mb-3">
  @if ($news[0]->banner)
<div style="max-height: 400px; overflow:hidden">
    <img src="{{ asset('storage/' . $news[0]->banner) }}"alt="{{ $news[0]->kategory->name }}" class="img-fluid">
</div>
@else
<img src="https://source.unsplash.com/1200x400?{{ $news[0]->kategory->name }}" class="card-img-top" alt="{{ $news[0]->kategory->name }}">
@endif
    
    <div class="card-body text-center">
      <h3 class="card-title"><a href="/news/{{ $news[0]->slug }}" class="text-decoration-none text-dark">{{ $news[0]->title }}</a></h3>
      <p><small class="text-muted">By. <a href="/news?author={{ $news[0]->author->username }}"class="text-decoration-none">{{ $news[0]->author->name }}</a>|<a href="/news?kategory={{ $news[0]->kategory->slug }}" class="text-decoration-none">{{ $news[0]->kategory->name }}</a> {{ $news[0]->created_at->diffForHumans() }}</small></p>
      <p class="card-text">{{ $news[0]->subtitle }}</p>
      <a href="/news/{{ $news[0]->slug }}" class="text-decoration-none btn btn-primary">Read More</a>
    </div>
  </div>


<div class="container">
    <div class="row">
        @foreach ($news->skip(1) as $new)
        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="position-absolute px-3 py-2 text-white" style="background-color: rgba(0,0,0,0.7)"><a href="/news?kategory={{ $new->kategory->slug }}" class="text-white text-decoration-none">{{ $new->kategory->name }}</a></div>
                @if ($new->banner)
                  <img src="{{ asset('storage/' . $new->banner) }}" alt="{{ $new->kategory->name }}" class="img-fluid">
                  </div>
                @else
                <img src="https://source.unsplash.com/500x400?{{ $new->kategory->name }}" class="card-img-top" alt="{{ $new->kategory->name }}">
                @endif 
                <div class="card-body">
                  <h5 class="card-title">{{ $new->title }}</h5>
                  <p><small class="text-muted">By. <a href="/news?author={{ $new->author->username }}"class="text-decoration-none">{{ $new->author->name }}</a> {{ $new->created_at->diffForHumans() }}</small></p>
                  <p class="card-text">{{ $new->subtitle }}</p>
                  <a href="/news/{{ $new->slug }}" class="btn btn-primary">Read More</a>
                </div>
              </div>
        </div>
        @endforeach
    </div>
</div>

@else
 <p class="text-center fs-4">No News found.</p>
@endif
    
<div class="d-flex justify-content-end"> 
{{ $news->links() }}
</div>
@endsection