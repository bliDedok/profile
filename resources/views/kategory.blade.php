@extends('halaman.main')

@section('container')
   <h1 class="mb-5">News Kategory : {{ $kategory }}</h1> 



@foreach ($news as $new)

            <h2>
                <a href="/news/{{ $news->slug }}">{{ $news->title }}</a>
            </h2>

            <h5>{{ $news->autor }}</h5>
            <p>{{ $news->excerpt }}</p>

@endforeach
@endsection