@extends('halaman.main')

@section('container')
   <h1 class="mb-5">News Categories<h1> 

    <div class="container">
        <div class="row">
            @foreach ($kategories as $kategory)
    
            <div class="col-md-4">
                <a href="/news?kategory={{ $kategory->slug }}">
                <div class="card text-bg-dark">
                    <img src="https://source.unsplash.com/500x500?{{ $kategory->name }}" class="card-img" alt="{{$kategory->name}}">
                    <div class="card-img-overlay d-flex align-items-center p-0">
                      <h5 class="card-title text-center flex-fill text-white p-4 fs-3" style="background-color: rgba(0, 0, 0, 0.7)">{{ $kategory->name }}</h5>
                    </div>
                  </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
@endsection