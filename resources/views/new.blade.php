

@extends('halaman.main')

@section('container')

<div class="container">
    <div class="row justify-content-center mb-5">
        <div class="col-md-8">
            <h1 class="mb-3">{{ $new->title }}</h1>
            <p>By. <a href="/news?author={{ $new->author->username }}"class="text-decoration-none">{{ $new->author->name }}</a>|<a href="/news?kategory={{ $new->kategory->slug }}" class="text-decoration-none">{{ $new->kategory->name }}</a></p>


            @if ($new->banner)
                 <img src="{{ asset('storage/' . $new->banner) }}" alt="{{ $new->kategory->name }}" class="img-fluid">
            @else
            <img src="https://source.unsplash.com/1200x400?{{ $new->kategory->name }}" alt="{{ $new->kategory->name }}" class="img-fluid">
            @endif 


<article class="my-3 fs-6">
{!! $new->news_content !!}
</article>



<a href="/news" class="d-block mt-3">Kembali ke halaman news</a>
        </div>
    </div>
</div>

@endsection