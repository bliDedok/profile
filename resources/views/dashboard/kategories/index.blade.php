@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">News Kategories</h1>
  </div>


  @if(session()->has('success'))
  <div class="alert alert-success col-lg-6" role="alert">
    {{ session('success') }}
  </div>
  @endif


  <div class="table-responsive col-lg-6">
    <a href="/dashboard/kategories/create" class="btn btn-primary mb-3" >Tambahkan Baru Kategory</a>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama Kategory</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($kategories as $kategory)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $kategory->name }}</td>
          <td><a href="/dashboard/kategories/{{ $kategory->slug }}"><span data-feather="eye" class=""></span></a>
          <a href="/dashboard/kategories/{{ $kategory->slug }}/edit"><span data-feather="edit" class="" style="color: orange"></span></a>
          <form action="/dashboard/kategories/{{ $kategory->slug }}" method="post" class="d-inline">
        @method('delete')
        @csrf
        <button><span data-feather="trash-2" class="border-0" style="color: red" onclick="return confirm('Apakah Kamu Yakin?')"></span></button>
    </form>
        </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
    
@endsection