@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">My News</h1>
  </div>


  @if(session()->has('success'))
  <div class="alert alert-success col-lg-8" role="alert">
    {{ session('success') }}
  </div>
  @endif


  <div class="table-responsive col-lg-8">
    <a href="/dashboard/news/create" class="btn btn-primary mb-3" >Tambahkan News Baru</a>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Title</th>
          <th scope="col">Kategory</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($news as $new)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $new->title }}</td>
          <td>{{ $new->kategory->name }}</td>
          <td><a href="/dashboard/news/{{ $new->slug }}"><span class="bi bi-eye-fill" style="text-decoration: none; font-size: 20px; color: blue;"></span>
          <a href="/dashboard/news/{{ $new->slug }}/edit"><span class="bi bi-pencil-square" style="text-decoration: none; font-size: 20px; color: goldenrod;"></span>
          <form action="/dashboard/news/{{ $new->slug }}" method="post" class="d-inline">
        @method('delete')
        @csrf
        <button><span class="bi bi-trash3" style="text-decoration: none; font-size: 20px; color: red;" onclick="return confirm('Apakah Kamu Yakin?')"></span></button>
        
    </form>
        </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
    
@endsection