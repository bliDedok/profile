@extends('dashboard.layouts.main')


@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Edit News</h1>
  </div>


  <div class="col-lg-8">
    <form method="post" action="/dashboard/news/{{ $news->slug }}" class="mb-5" enctype="multipart/form-data">
        @method('put')
        @csrf
    <div class="mb-3">
      <label for="title" class="form-label @error ('title') is-invalid @enderror">Title</label>
      <input type="text" class="form-control" id="title" name="title" required autofocus value="{{ old('title', $news->title) }}">
      @error('title')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="slug" class="form-label  @error ('slug') is-invalid @enderror">Slug</label>
      <input type="text" class="form-control" id="slug" name="slug" required value="{{ old('slug', $news->slug) }}">
      @error('slug')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>

    <div class="mb-3">
      <label for="kategory" class="form-label">Kategory</label>
      <select class="form-select" name="kategory_id">
        @foreach ($kategories as $kategory)
        @if(old('kategory_id', $news->kategory_id) == $kategory->id)
        <option value="{{ $kategory->id }}" selected>{{ $kategory->name }}</option>
        @else
        <option value="{{ $kategory->id }}">{{ $kategory->name }}</option>
        @endif
        @endforeach
      </select>
    </div>

    <div class="mb-3">
      <label for="banner" class="form-label">News Banner</label>
      <input type="hidden" name="oldImage" value="{{ $news->banner }}">
      @if($news->banner)
      <img src="{{ asset('storage/' . $news->banner) }}" class="img-preview img-fluid mb-3 col-sm-5 d-block">
      @else
      <img class="img-preview img-fluid mb-3 col-sm-5">
      @endif
      <input class="form-control @error ('banner') is-invalid @enderror" type="file" id="banner" name="banner" onchange="previewImage()">
      @error('banner')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>

    <div class="mb-3">
      <label for="news_content" class="form-label">News Content</label>
      @error('news_content')
        <p style="color: red"> {{ $message }}</p>
        @enderror
      <input id="news_content" type="hidden" name="news_content" value="{{old('news_content', $news->news_content)}}">
    <trix-editor input="news_content"></trix-editor>

    </div>


    <button type="submit" class="btn btn-primary">Update News</button>
  </form>
</div>
  
 <script>
    const title = document.querySelector('#title');
    const slug = document.querySelector('#slug');


    title.addEventListener('change', function(){
        fetch('/dashboard/news/cekSlug?title=' + title.value)
        .then(response => response.json())
        .then(data => slug.value = data.slug)
    });

    document.addEventListener('trix-file-accept', function(e){
        e.preventDefault();
    });

    function previewImage() {
        const image = document.querySelector('#image');
        const imgPreview = document.querySelector('.img-preview');

        imgPreview.style.display = 'block';

        const ofReader = new FileReader();
        ofReader.readAsDataURL(image.files[0]);

        ofReader .onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
        }
      }
 </script>


@endsection