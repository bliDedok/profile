<?php

namespace Database\Seeders;
use App\Models\User;
use App\Models\Kategory;
use App\Models\News;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            'name' => 'Dedy Wanditya',
            'username' => 'DedyWanditya',
            'email' => 'made@gmail.com',
            'password' => bcrypt('12345')
        ]);

        
        User::factory(3)->create();
        // Kategory::factory(3)->create();

        Kategory::create([
            'user_id' => mt_rand(1,3),
            'name' => 'Web Programming',
            'slug' => 'web-programming',
            'category_description' => 'web-Programming'
            
        ]);
        
        Kategory::create([
            'user_id' => mt_rand(1,3),
            'name' => 'Personal',
            'slug' => 'personal',
            'category_description' => 'personal'
            
        ]);
        Kategory::create([
            'user_id' => mt_rand(1,3),
            'name' => 'Web Design',
            'slug' => 'web-design',
            'category_description' => 'web-design'
        ]);


        News::factory(20)->create();
        
        
    }
}
