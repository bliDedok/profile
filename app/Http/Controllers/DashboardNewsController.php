<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Kategory;
// use Illuminate\Http\Request;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class DashboardNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.news.index', [
            'news' => News::where('user_id', auth()->user()->id)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view ('dashboard.news.create', [
            'kategories' => Kategory::all()
        ]);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required|max:255',  
            'slug' => 'required|unique:news',
            'kategory_id' => 'required',
            'banner' => 'image|file|max:1024',
            'news_content' => 'required'
        ]);

        if($request->file('banner')){
            $validateData['banner'] = $request->file('banner')->store('post-images');
        }


        $validateData['user_id'] = auth()->user()->id;
        $validateData['subtitle'] = Str::limit(strip_tags($request->news_content), 200);

        News::create($validateData);    

        return redirect('/dashboard/news')->with('success', 'Postingan baru sudah ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        return view('dashboard.news.show',[
            'news' => $news

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view ('dashboard.news.edit', [
            'news' => $news,
            'kategories' => Kategory::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $rules = [
            'title' => 'required|max:255',  
            'kategory_id' => 'required',
            'banner' => 'image|file|max:1024',
            'news_content' => 'required'
        ];

        

        if($request->slug != $news->slug) {

            $rules['slug'] = 'required|unique:news';
 
        }

        $validateData = $request->validate($rules);

        if($request->file('banner')){
            if($request->oldImage){
                Storage::delete($request->oldImage);
            }
            $validateData['banner'] = $request->file('banner')->store('post-images');
        }

        $validateData['user_id'] = auth()->user()->id;
        $validateData['subtitle'] = Str::limit(strip_tags($request->news_content), 200);

        News::where('id', $news->id)
            ->update($validateData);    

        return redirect('/dashboard/news')->with('success', 'Postingan baru sudah diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        
        if($news->banner){
            Storage::delete($news->banner);
        }

    News::destroy($news->id);    


    return redirect('/dashboard/news')->with('success', 'Postingan Berhasil Dihapus');
}

public function cekSlug(Request $request)
{
    $slug = SlugService::createSlug(News::class, 'slug', $request->title);
    return response()->json(['slug' => $slug]); 

    }
}
