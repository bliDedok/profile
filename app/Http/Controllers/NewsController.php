<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\News;
use App\models\User;
use App\models\kategory;


class NewsController extends Controller
{
    public function index()
    {

        $title = '';
        if(request('kategory')){
            $kategory = Kategory::firstwhere('slug', request('kategory'));
            $title = ' in ' . $kategory->name;
        }

        if(request('author')) {
            $author = User::firstWhere('username', request('author'));
            $title = ' by ' .$author->name;
        }
        return view('news',[
            "title" => "All News" . $title,
            // "posts" => post::all()
            "news" => News::latest()->filter(request(['search', 'kategory','author']))->paginate(7)->withQueryString()
       
       ]);
    }


    public function show(news $new)
    {
        return view('new',[
            "title" => "single news",
            "new" => $new
        ]);
    }
}

