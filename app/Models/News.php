<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class News extends Model
{
    use HasFactory, sluggable;



    // protected $fillable = ['title', 'excerpt','body'];
    protected $guarded = ['id'];

    protected $with = ['kategory', 'author'];


    public function scopeFilter($query, array $filters)
    {
        

        $query->when($filters['search'] ?? false, function ($query, $search){
            return $query->where('title', 'like', '%' . $search . '%')
                    ->orWhere('news_content', 'like', '%' . $search . '%');
        });

        $query->when($filters['kategory'] ?? false, function($query, $kategory){
            return $query->whereHas('kategory', function($query) use ($kategory){
                $query->where('slug',$kategory);
            });
        });


        $query->when($filters['author'] ?? false, fn($query, $author) => 
         $query->whereHas('author', fn($query) => 
            $query->where('username', $author) 
         )
    );
    }


    public function kategory()
    {
        return $this->belongsTo(Kategory::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


public function getRouteKeyName()
{
    return 'slug';
}

public function sluggable(): array
{
    return[
        'slug' => [
            'source' => 'title'
        ]
        ];
}


}
