<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategory extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    
    

    public function news()
    {
        return $this->hasMany(News::class);
    }

    // public function author()
    // {
    //     return $this->belongsTo(User::class, 'user_id');
    // }
}
